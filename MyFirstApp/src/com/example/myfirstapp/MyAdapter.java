package com.example.myfirstapp;

import android.widget.BaseAdapter;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {
  final private ArrayList<String> data;
  final private LayoutInflater inflater;

  public MyAdapter(Context context, ArrayList<String> data) {
    this.data = data;
    this.inflater = LayoutInflater.from(context);
  }

  @Override
  public int getCount() {
    return data.size();
  }

  @Override
  public Object getItem(int position) {
    return data.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  public View getView(int position, View convertView, ViewGroup parent) {
    View view;
    if (convertView == null) {
      // create view
      view = inflater.inflate(R.layout.row, null);
    } else {
      // reuse
      view = convertView;
    }

    String text = (String) getItem(position);
    ((TextView) view.findViewById(R.id.title)).setText(text);
    return view;
  }
}

package com.example.myfirstapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import java.util.ArrayList;
import android.widget.AdapterView;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;
import android.widget.Button;
import java.util.Date;

public class MainActivity extends Activity {
    private MyAdapter adapter;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.main);


      final ArrayList<String> data = getData();
      adapter = new MyAdapter(this, data);

      final ListView listview = (ListView) findViewById(R.id.listview);
      listview.setAdapter(adapter);
      listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parentAdapter, View view, int position, long id) {
          TextView clickedView = (TextView) view.findViewById(R.id.title);
          String message = "Item with id ["+id+"] - Position ["+position+"]: [" +clickedView.getText()+"]";
          Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

      });

      //final Button addEntriesBtn = (Button) findViewById(R.id.add_entries_btn);
      //addEntriesBtn.setOnClickListener(new View.OnClickListener() {
        //public void onClick(View v) {
          //data.add("new entry " + (new Date()).toString());
        //}
      //});


    }

    private ArrayList<String> getData() {
      ArrayList<String> data = new ArrayList<String>();
      data.add("entry 1");
      data.add("entry 2");
      return data;
    }

}
